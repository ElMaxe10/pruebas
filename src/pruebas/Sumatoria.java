package pruebas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Sumatoria {

	private JFrame frame;
	private JTextField num1;
	private JTextField num2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Sumatoria window = new Sumatoria();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Sumatoria() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 426, 218);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblListadoDeNumeros = new JLabel("Listado de Numeros y Sumatoria");
		lblListadoDeNumeros.setHorizontalAlignment(SwingConstants.CENTER);
		lblListadoDeNumeros.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblListadoDeNumeros.setBounds(10, 11, 414, 36);
		frame.getContentPane().add(lblListadoDeNumeros);
		
		JLabel lblListadoDeNumeros_1 = new JLabel("Listado de Numeros");
		lblListadoDeNumeros_1.setFont(new Font("Times New Roman", Font.PLAIN, 12));
		lblListadoDeNumeros_1.setBounds(24, 114, 119, 14);
		frame.getContentPane().add(lblListadoDeNumeros_1);
		
		JLabel lblSumatoria = new JLabel("Sumatoria");
		lblSumatoria.setFont(new Font("Times New Roman", Font.PLAIN, 12));
		lblSumatoria.setBounds(271, 162, 69, 14);
		frame.getContentPane().add(lblSumatoria);
		
		JLabel lbl_sumatoria = new JLabel("");
		lbl_sumatoria.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_sumatoria.setBounds(326, 162, 74, 14);
		frame.getContentPane().add(lbl_sumatoria);
		
		JLabel lbl_listado = new JLabel("");
		lbl_listado.setFont(new Font("Times New Roman", Font.PLAIN, 12));
		lbl_listado.setHorizontalAlignment(SwingConstants.LEFT);
		lbl_listado.setBounds(34, 132, 363, 20);
		frame.getContentPane().add(lbl_listado);
		
		num1 = new JTextField();
		num1.setBounds(85, 58, 86, 20);
		frame.getContentPane().add(num1);
		num1.setColumns(10);
		
		num2 = new JTextField();
		num2.setBounds(254, 58, 86, 20);
		frame.getContentPane().add(num2);
		num2.setColumns(10);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try{
					int n1= new Integer (num1.getText());
					int n2= new Integer (num2.getText());
					if(n1>n2)	//el primer numero no puede ser mayor al segundo
						throw new IllegalArgumentException();
					int suma= 0;
					String cadena= "";
					
					for(int n=n1; n<=n2; n++){
						suma= suma + n;
						cadena= cadena + n + ", ";
					}
					lbl_listado.setText(cadena);
					lbl_sumatoria.setText(String.valueOf(suma));
					num1.setText(null);
					num2.setText(null);
				}
				catch(Exception ee){
					JOptionPane.showMessageDialog(null, "Hay un error con el ingreso de los numeros");
				}
			}
		});
		btnAceptar.setToolTipText("Aprietame");
		btnAceptar.setBounds(163, 89, 89, 23);
		frame.getContentPane().add(btnAceptar);
	}

}
