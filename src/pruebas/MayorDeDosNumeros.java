package pruebas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MayorDeDosNumeros {

	private JFrame frame;
	private JTextField num1;
	private JTextField num2;
	private JLabel lblNumero_1;
	private JButton btnNewButton;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MayorDeDosNumeros window = new MayorDeDosNumeros();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MayorDeDosNumeros() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 283, 184);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblMayorDeDos = new JLabel("Mayor de Dos Numeros");
		lblMayorDeDos.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblMayorDeDos.setBounds(26, 11, 218, 48);
		frame.getContentPane().add(lblMayorDeDos);
		
		num1 = new JTextField();
		num1.setBounds(36, 70, 86, 20);
		frame.getContentPane().add(num1);
		num1.setColumns(10);
		
		num2 = new JTextField();
		num2.setBounds(152, 70, 86, 20);
		frame.getContentPane().add(num2);
		num2.setColumns(10);
		
		JLabel lblNumero = new JLabel("numero 1");
		lblNumero.setBounds(36, 55, 86, 14);
		frame.getContentPane().add(lblNumero);
		
		lblNumero_1 = new JLabel("numero 2");
		lblNumero_1.setBounds(158, 55, 86, 14);
		frame.getContentPane().add(lblNumero_1);
		
		btnNewButton = new JButton("OK");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					int n1= new Integer(num1.getText());
					int n2= new Integer(num2.getText());
					int mayor;
					if(n1>n2)
						mayor= n1;
					else mayor= n2;
					if(n1==n2)
						JOptionPane.showMessageDialog(null, "Los numeros son iguales");
					else JOptionPane.showMessageDialog(null, "El mayor es: " + mayor);
					num1.setText(null);
					num2.setText(null);
				}
				catch(Exception ee){
					JOptionPane.showMessageDialog(null, "Le falta ingresar algun numero");
				}
			}
		});
		btnNewButton.setBounds(90, 101, 89, 23);
		frame.getContentPane().add(btnNewButton);
	}

}
