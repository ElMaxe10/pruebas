package pruebas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Suma {

	private JFrame frame;
	private JTextField num1;
	private JTextField num2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Suma window = new Suma();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Suma() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 171);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblSumaDeDosNumeros = new JLabel("Suma de dos Numeros");
		lblSumaDeDosNumeros.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblSumaDeDosNumeros.setBounds(98, 11, 230, 25);
		frame.getContentPane().add(lblSumaDeDosNumeros);
		
		num1 = new JTextField();
		num1.setBounds(81, 60, 86, 20);
		frame.getContentPane().add(num1);
		num1.setColumns(10);
		
		num2 = new JTextField();
		num2.setBounds(259, 60, 86, 20);
		frame.getContentPane().add(num2);
		num2.setColumns(10);
		
		JLabel lblNumero = new JLabel("Numero 1");
		lblNumero.setBounds(81, 47, 86, 14);
		frame.getContentPane().add(lblNumero);
		
		JLabel lblNumero_1 = new JLabel("Numero 2");
		lblNumero_1.setBounds(259, 47, 86, 14);
		frame.getContentPane().add(lblNumero_1);
		
		JButton btnCalcular = new JButton("CALCULAR");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					int n1= new Integer(num1.getText());
					int n2= new Integer(num2.getText());
					JOptionPane.showMessageDialog(null, "El resultado de la suma es: " + (n1+n2) );
					num1.setText(null);
					num2.setText(null);
				}
				catch(Exception ee){
					JOptionPane.showMessageDialog(null, "Le falta ingresar algun numero");
				}
			}
		});
		btnCalcular.setBounds(154, 91, 114, 23);
		frame.getContentPane().add(btnCalcular);
	}
}
