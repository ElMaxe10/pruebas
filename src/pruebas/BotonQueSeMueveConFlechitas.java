package pruebas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class BotonQueSeMueveConFlechitas {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BotonQueSeMueveConFlechitas window = new BotonQueSeMueveConFlechitas();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public BotonQueSeMueveConFlechitas() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnNewButton = new JButton("New button");
		btnNewButton.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
		          switch (e.getKeyCode()) {
		            case KeyEvent.VK_LEFT:
		              moverIzq();
		              break;
		            case KeyEvent.VK_RIGHT:
		              moverDer();
		              break;
		            case KeyEvent.VK_DOWN:
		              moverAbajo();
		              break;
		            case KeyEvent.VK_UP:
		              moverArriba();
		              break;
		          }
			}

			private void moverArriba() {
				btnNewButton.setLocation(btnNewButton.getX(), btnNewButton.getY()-10);
			}

			private void moverAbajo() {
				btnNewButton.setLocation(btnNewButton.getX(), btnNewButton.getY()+10);
			}

			private void moverDer() {
				btnNewButton.setLocation(btnNewButton.getX()+10, btnNewButton.getY());
			}

			private void moverIzq() {
				btnNewButton.setLocation(btnNewButton.getX()-10, btnNewButton.getY());
			}
		});
		btnNewButton.setBounds(142, 102, 162, 23);
		frame.getContentPane().add(btnNewButton);
	}
	
	
}
